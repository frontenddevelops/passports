import com.h2osis.auth.Role
import com.h2osis.auth.User
import com.h2osis.auth.UserRole
import com.h2osis.constant.AuthKeys

class BootStrap {

    def init = { servletContext ->

        if (Role.count() != 2) {
            new Role(authority: AuthKeys.ADMIN, description: "admin").save(flush: true)
            new Role(authority: AuthKeys.USER, description: "user").save(flush: true)
        }

        if (!User.count()) {
            User user = new User(username: "h2osis", password: "123", email: "sokolovep@gmail.com", firstname: "egor", secondname: "sokolov", phone: "+7(904)238-79-70").save(flush: true)
            Role role = Role.findByAuthority(AuthKeys.ADMIN)
            new UserRole(user: user, role: role).save(flush: true)
        }
    }
    def destroy = {
    }
}
