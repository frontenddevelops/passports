import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import {HeroService} from "./hero.service";
import {DashboardComponent} from "./dashboard.component";
import {HeroDetailedComponent} from "./hero-detailed.component";
import {HeroesComponent} from "./heroes.component";
import { AppRoutingModule } from "./app-routing.module";
import {HeroSearchComponent} from "./hero-search.component";

@NgModule({
  imports:      [ BrowserModule,
                  FormsModule,
                  AppRoutingModule,
                  HttpModule,
                  InMemoryWebApiModule.forRoot(InMemoryDataService),

  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroDetailedComponent,
    HeroesComponent,
    HeroSearchComponent

  ],
  bootstrap:    [ AppComponent ],
  providers: [HeroService]

})



export class AppModule { }
